(require 'package)
(setq package-archives '(("stable-melpa" . "https://stable.melpa.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))

(setq package-enable-at-startup nil)
(package-initialize nil)

(unless (package-installed-p 'use-package)
  (message "EMACS install use-package.el")
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory) user-emacs-directory)
        ((boundp 'user-init-directory) user-init-directory)
        (t "~/.emacs.d/")))

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(load-user-file "themes.el")
(load-user-file "magit.el")
(load-user-file "edit-server.el")
(load-user-file "expand-region.el")
;(load-user-file "rust.el")
(load-user-file "org.el")
(load-user-file "python.el")
(load-user-file "lsp.el")
;;(load-user-file "yasnippet.el")
;;(load-user-file "java.el")
;;(load-user-file "lsp-java.el")

(setq custom-file "~/.emacs.d/customize.el")
(load-user-file "customize.el")

;; (require 'lsp-java)
;; (add-hook 'java-mode-hook #'lsp-java-enable)

;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(package-selected-packages
;;    (quote
;;     (realgud lsp-java treemacs treemacs-projectile material-theme))))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )
