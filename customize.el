(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(lsp-java-server-install-dir "/home/camel/Downloads/jdt/")
 '(package-selected-packages
   '(magit find-file-in-project java-snippets company exec-path-from-shell rustic telega idle-highlight-mode flycheck idle-highlight-in-visible-buffers-mode dap-mode lsp-ui projectile idle-highlight expand-region edit-server feature-mode ace-jump-mode rainbow-delimiters emmet-mode realgud-jdb realgud arduino-mode slime lsp-java company-lsp lsp-mode yasnippet treemacs material-theme use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
