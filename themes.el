;;; Some themes and decorations.

(menu-bar-mode -1)
(tool-bar-mode -1)

(save-place-mode 1)
(setq use-dialog-box nil)
(global-auto-revert-mode 1)

(set-frame-font "Hack 16")

(use-package doom-themes
  :init (load-theme 'doom-dracula t))

(use-package doom-modeline
  :init (doom-modeline-mode 1))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

(column-number-mode)
(global-display-line-numbers-mode t)
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default c-basic-offset 4)
(setq c-default-style "k&r"
      c-basic-offset 4)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(require 'whitespace)
(setq-default whitespace-style '(face tabs trailing spaces
                                      space-before-tab newline empty
                                      space-after-tab space-mark
                                      tab-mark newline-mark))
(setq whitespace-display-mappings
     ;; all numbers are Unicode codepoint in decimal. try (insert-char 182 ) to see it
     '(
       (space-mark 32 [183] [46]) ; 32 SPACE, 183 MIDDLE DOT, 46 FULL STOP
       ;;(space-mark 32 [11825] [46]) ; 32 SPACE, 183 WORD SEPARATOR MIDDLE DOT, 46 FULL STOP
       (newline-mark 10 [182 10]) ; 10 LINE FEED
       ;;(tab-mark 9 [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE
       ;;(tab-mark 9 [8594 9] [92 9]) ; 9 TAB, 8594 RIGHTWARDS ARROW
       ;;(tab-mark 9 [9144 9] [92 9]) ; 9 TAB, LEFT VER­TI­CAL BOX LINE
       (tab-mark 9 [9474 9] [92 9]) ; 9 TAB, BOX DRAWING LIGHT VERTICAL
       ))
(global-whitespace-mode 1)
